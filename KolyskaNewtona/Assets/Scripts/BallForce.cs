﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallForce : MonoBehaviour  {

    Rigidbody rb;

    void Start () {
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {

        
    }

    void OnMouseDrag()
    {
         var mousePosition = Input.mousePosition;
        mousePosition.z = 0;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

         rb.MovePosition(mousePosition);
    }
}
