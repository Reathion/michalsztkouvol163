﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerManager : MonoBehaviour {
    public Text textNumber;

    [HideInInspector]
    public bool ShouldCreateTower = true;
    int maxTower = 100;
    int towerNumber = 0;
    List<TowerBehaviour> towers = new List<TowerBehaviour>();

	// Use this for initialization
	void Start () {
		
	}
	
	public void AddTower(TowerBehaviour tower)
    {
        towers.Add(tower);

        towerNumber++;
        if (towerNumber > maxTower)
            ActivateAllTowerAgain();
        LogTower();
    }

    public void ActivateAllTowerAgain()
    {
        ShouldCreateTower = false;
        foreach (var item in towers)
        {
            item.ActivateTowerAgain();
        }
    }
    public void RemoveTower(TowerBehaviour tower)
    {
        if (towers.Contains(tower))
        {
            int removeIndex = towers.FindIndex(x => x == tower);
            towers.RemoveAt(removeIndex);
            towerNumber--;
            LogTower();
        }
    }

    void LogTower()
    {
        textNumber.text = "Towers: " + towerNumber.ToString();
    }
}
