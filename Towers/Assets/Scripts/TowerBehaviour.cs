﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehaviour : MonoBehaviour {

    public GameObject BulletPrefab;
    public bool IsFirstTower = false;

    float shootTime = 0.5f;
    float snoozeTime = 6;
    int rotateMin = 15;
    int rotateMax = 45;
    int howManyShoots = 12;

    int shootCounter = 0;
    float currentTime = 0;


    BulletBehaviour currentBullet;
    TowerManager TM;
    SpriteRenderer towerSprite;
    // Use this for initialization
    void Start () {

        TM = GetComponentInParent<TowerManager>();
        TM.AddTower(this);

        towerSprite = GetComponent<SpriteRenderer>();

        shootCounter = howManyShoots;
        if (IsFirstTower)
            snoozeTime = 0;
    }
	
	// Update is called once per frame
	void Update () {
        currentTime += Time.deltaTime;
        if (currentTime > snoozeTime)
        {
            towerSprite.color = shootCounter > 0 ? Color.red : Color.white;
            snoozeTime = 0;
            if (currentTime > shootTime)
            {
                currentTime = 0;
                if (shootCounter > 0)
                {
                    Shoot();
                }
                else
                {
                    return;
                }
                shootCounter--;
            }
        }
    }

    public void ActivateTowerAgain()
    {
        shootCounter = howManyShoots;
        snoozeTime = 0;
    }

    void Shoot()
    {
        this.transform.Rotate(new Vector3(0, 0, Random.Range(rotateMin, rotateMax + 1)));
        currentBullet = Instantiate(BulletPrefab, this.transform.position, this.transform.rotation, TM.transform).GetComponent<BulletBehaviour>();
    }

   void OnTriggerEnter2D(Collider2D col)
    {
        BulletBehaviour bb = col.gameObject.GetComponent<BulletBehaviour>();
        if (bb!= null && bb != currentBullet)
        {
            Destroy(bb.gameObject);
            TM.RemoveTower(this);
            Destroy(this.gameObject);
        }
    }
}
