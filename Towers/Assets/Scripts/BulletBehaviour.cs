﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {

    public GameObject TowerPrefab;

    Vector3 destination;
    float speed = 1f;
    int minDist = 1;
    int maxDist = 4;

    TowerManager TM;
	// Use this for initialization
	void Start () {
        TM = GetComponentInParent<TowerManager>();
        float distance = Random.Range(minDist, maxDist + 1);
        destination = transform.position + (transform.up * distance);
	}

    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, destination, step);

        if (transform.position == destination)
        {
            if (TM.ShouldCreateTower)
                Instantiate(TowerPrefab,this.transform.position, new Quaternion(), TM.transform);
            Destroy(this.gameObject);
        }
    }
}
